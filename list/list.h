#ifndef QUARTZ_STRUCTURES_LIST_H
#define QUARTZ_STRUCTURES_LIST_H

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

typedef struct qz_list_node qz_List_Node;
typedef struct qz_list qz_List;

struct qz_list_node {
    void *info;
    struct qz_list_node *next;
    struct qz_list_node *prev;
};

struct qz_list {
    qz_List_Node *head;
    qz_List_Node *tail;

    int size;
    size_t info_size;
};

qz_List* qz_list_create(size_t info_size);
void qz_list_destroy(qz_List *list);

void qz_list_append(qz_List *list, void *info);
void qz_list_insert(qz_List *list, void *info);
void qz_list_remove(qz_List *qzll, int position);
void qz_list_get(qz_List *qzll, int position, void **resp);
void qz_list_pop(qz_List *qzll, int position, void **resp);

int qz_list_size(qz_List *list);

#endif //QUARTZ_STRUCTURES_LIST_H
