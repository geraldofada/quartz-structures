#include "list.h"

// PRIVATE DECLARATION
qz_List_Node* get_nth_node(qz_List *qzll, int position);


// PUBLIC
qz_List* qz_list_create(size_t info_size) {
    qz_List *qzll = (qz_List*) malloc(sizeof(qz_List));

    qzll->head = NULL;
    qzll->tail = NULL;
    qzll->size = 0;
    qzll->info_size = info_size;

    return qzll;
}

void qz_list_append(qz_List *qzll, void *info) {
    qz_List_Node *to_append = (qz_List_Node*) malloc(sizeof(qz_List_Node));
    to_append->next = NULL;
    to_append->prev = NULL;
    to_append->info = malloc(qzll->info_size);

    memmove(to_append->info, info, qzll->info_size);

    if (qzll->head == NULL) {
        qzll->head = to_append;
        qzll->tail = to_append;
    } else {
        to_append->prev = qzll->tail;
        qzll->tail->next = to_append;
        qzll->tail = to_append;
    }

    qzll->size++;
}

void qz_list_insert(qz_List *qzll, void *info) {
    qz_List_Node *to_insert = (qz_List_Node*) malloc(sizeof(qz_List_Node));
    to_insert->next = NULL;
    to_insert->prev = NULL;
    to_insert->info = malloc(qzll->info_size);

    memmove(to_insert->info, info, qzll->info_size);

    if (qzll->head == NULL) {
        qzll->head = to_insert;
        qzll->tail = to_insert;
    } else {
        to_insert->next = qzll->head;
        qzll->head->prev = to_insert;
        qzll->head = to_insert;
    }

    qzll->size++;
}

void qz_list_remove(qz_List *qzll, int position) {
    // Check if it is boundary
    if (position < 0 || position >= qzll->size) return;

    qz_List_Node *aux = get_nth_node(qzll, position);

    // Rework the list pointers
    if (aux->prev == NULL) {
        qzll->head = aux->next;
        aux->next->prev = NULL;
    } else if (aux->next == NULL) {
        qzll->tail = aux->prev;
        aux->prev->next = NULL;
    } else {
        aux->next->prev = aux->prev;
        aux->prev->next = aux->next;
    }

    // Free memory and resize the list
    qzll->size--;
    free(aux->info);
    free(aux);
}

void qz_list_get(qz_List *qzll, int position, void **resp) {
    // Check if it is boundary
    if (position < 0 || position >= qzll->size) return;

    qz_List_Node *aux = get_nth_node(qzll, position);

    // Set the response
    *resp = malloc(qzll->info_size);
    memmove(*resp, aux->info, qzll->info_size);
}

void qz_list_pop(qz_List *qzll, int position, void **resp) {
    // Check if it is boundary
    if (position < 0 || position >= qzll->size) return;

    qz_list_get(qzll, position, resp);
    qz_list_remove(qzll, position);
}

int qz_list_size(qz_List *list) {
    return list->size;
}

void qz_list_destroy(qz_List *qzll) {
    qz_List_Node *aux = qzll->head;
    while (qzll->head != NULL) {
        qzll->head = qzll->head->next;
        free(aux->info);
        free(aux);
        aux = qzll->head;
    }

    free(qzll);
}


// PRIVATE IMPLEMENTAION
qz_List_Node* get_nth_node(qz_List* qzll, int position) {
    qz_List_Node *aux = qzll->head;
    for (int i = 0; i < position; i++) {
        aux = aux->next;
    }

    return aux;
}
