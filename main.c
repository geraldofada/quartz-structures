#include <stdlib.h>
#include <stdio.h>

#include "list/list.h"
#include "bst/bst.h"

void ll_print(qz_List *qzll, int start, int end) {
    void *resp = NULL;
    for (int i = start; i < end; i++) {
        qz_list_get(qzll, i, &resp);
        printf("[%i]->", *(int*)resp);
        free(resp);
    }
}

qz_Bst_Compare_Result bst_int_comp(void *to, void *being) {
    if (*(int*)to > *(int*)being)
        return QZ_BST_GREATER;
    else if (*(int*)to < *(int*)being)
        return QZ_BST_LESS;
    else
        return QZ_BST_EQUAL;
}

void bst_print(qz_Bst_Node *qzbst) {
    if (qzbst != NULL) {
        bst_print(qzbst->left);
        printf("%i ", *(int*)qzbst->info);
        bst_print(qzbst->right);
    }
}

int main() {
    qz_Bst *bst = qz_bst_create(sizeof(int), &bst_int_comp);

    int a = 9;
    qz_bst_insert(bst, &a);

    a = 8;
    qz_bst_insert(bst, &a);

    a = 10;
    qz_bst_insert(bst, &a);

    a = 11;
    qz_bst_insert(bst, &a);

    a = 6;
    qz_bst_insert(bst, &a);


    a = 10;
    qz_Bst *new = qz_bst_get(bst, &a);

    a = 3;
    qz_bst_insert(new, &a);

    void *resp = NULL;
    qz_bst_get_info(new, &a, &resp);
    printf("%i", *(int*)resp);
    free(resp);

    a = 9;
    printf("%i", qz_bst_exits_info(new, &a));

    qz_bst_destroy(bst);
    qz_bst_destroy(new);

    return 0;
}
