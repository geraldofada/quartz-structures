#include "bst.h"

// ------------------
// PRIVATE
// ------------------
qz_Bst_Node* create_node(
        void *info,
        size_t info_size,
        qz_Bst_Node *left,
        qz_Bst_Node *right
) {
    qz_Bst_Node *qzbst_n = malloc(sizeof(qz_Bst_Node));
    qzbst_n->info = malloc(info_size);
    qzbst_n->left = left;
    qzbst_n->right = right;

    memmove(qzbst_n->info, info, info_size);

    return qzbst_n;
}

qz_Bst_Node* insert_node(
        qz_Bst_Node *qzbst_n,
        void *info,
        size_t info_size,
        qz_Bst_Compare_Func compare_func
) {
    if (qzbst_n == NULL)
        return create_node(info, info_size, NULL, NULL);

    if (compare_func(qzbst_n->info, info) == QZ_BST_GREATER)
        qzbst_n->left = insert_node(qzbst_n->left, info, info_size, compare_func);
    else if (compare_func(qzbst_n->info, info) == QZ_BST_LESS)
        qzbst_n->right = insert_node(qzbst_n->right, info, info_size, compare_func);

    return qzbst_n;
}

qz_Bst_Node* find_node(void *info, qz_Bst_Node *qzbst_n, qz_Bst_Compare_Func compare_func) {
    if (qzbst_n == NULL)
        return NULL;

    if (compare_func(qzbst_n->info, info) == QZ_BST_LESS)
        return find_node(info, qzbst_n->right, compare_func);
    if (compare_func(qzbst_n->info, info) == QZ_BST_GREATER)
        return find_node(info, qzbst_n->left, compare_func);

    return qzbst_n;
}

qz_Bst_Node* find_node_max(qz_Bst_Node *qzbst_n) {
    if (qzbst_n == NULL) return NULL;
    if (qzbst_n->right == NULL) return qzbst_n;
    return find_node_max(qzbst_n->right);
}

qz_Bst_Node* find_node_min(qz_Bst_Node *qzbst_n) {
    if (qzbst_n == NULL) return NULL;
    if (qzbst_n->left == NULL) return qzbst_n;
    return find_node_min(qzbst_n->left);
}

void destroy_node(qz_Bst_Node *qzbst_n) {
    if (qzbst_n != NULL) {
        destroy_node(qzbst_n->left);
        destroy_node(qzbst_n->right);
        free(qzbst_n->info);
        free(qzbst_n);
    }
}

qz_Bst_Node* copy(qz_Bst_Node *qzbst, size_t info_size) {
    if (qzbst == NULL)
        return NULL;

    qz_Bst_Node *copied = create_node(
            qzbst->info,
            info_size,
            copy(qzbst->left, info_size),
            copy(qzbst->right, info_size)
    );

    return copied;
}

// ------------------
// PUBLIC
// ------------------
qz_Bst* qz_bst_create(size_t info_size, qz_Bst_Compare_Func compare_func) {
    qz_Bst *qzbst = malloc(sizeof(qz_Bst));
    qzbst->head = NULL;
    qzbst->info_size = info_size;
    qzbst->compare_func = compare_func;

    return qzbst;
}

void qz_bst_insert(qz_Bst *qzbst, void *info) {
    qzbst->head = insert_node(qzbst->head, info, qzbst->info_size, qzbst->compare_func);
}

void qz_bst_destroy(qz_Bst *qzbst) {
    destroy_node(qzbst->head);
    free(qzbst);
}

void qz_bst_get_info(qz_Bst *qzbst, void *info, void **resp) {
    qz_Bst_Node *found_head = find_node(info, qzbst->head, qzbst->compare_func);

    if (found_head != NULL) {
        *resp = malloc(qzbst->info_size);
        memmove(*resp, found_head->info, qzbst->info_size);
    } else {
        *resp = NULL;
    }
}

void qz_bst_get_info_max(qz_Bst *qzbst, void **resp) {
    qz_Bst_Node *found_head_max = find_node_max(qzbst->head);

    if (found_head_max != NULL) {
        *resp = malloc(qzbst->info_size);
        memmove(*resp, found_head_max->info, qzbst->info_size);
    } else {
        *resp = NULL;
    }
}

void qz_bst_get_info_min(qz_Bst *qzbst, void **resp) {
    qz_Bst_Node *found_head_min = find_node_min(qzbst->head);

    if (found_head_min != NULL) {
        *resp = malloc(qzbst->info_size);
        memmove(*resp, found_head_min->info, qzbst->info_size);
    } else {
        *resp = NULL;
    }
}

bool qz_bst_exits_info(qz_Bst *qzbst, void *info) {
    qz_Bst_Node *found_head = find_node(info, qzbst->head, qzbst->compare_func);

    if (found_head != NULL)
        return true;
    else
        return false;
}

qz_Bst* qz_bst_get(qz_Bst *qzbsts, void *info) {
    qz_Bst_Node *found_head = find_node(info, qzbsts->head, qzbsts->compare_func);
    qz_Bst_Node *copied_head = copy(found_head, qzbsts->info_size);

    qz_Bst *found_tree = qz_bst_create(qzbsts->info_size, qzbsts->compare_func);
    found_tree->head = copied_head;

    return found_tree;
}

qz_Bst* qz_bst_copy(qz_Bst *qzbst) {
    qz_Bst_Node *copied_head = copy(qzbst->head, qzbst->info_size);
    qz_Bst* copied_tree = qz_bst_create(qzbst->info_size, qzbst->compare_func);
    copied_tree->head = copied_head;

    return copied_tree;
}
