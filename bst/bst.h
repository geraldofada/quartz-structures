#ifndef QUARTZ_STRUCTURES_BST_H
#define QUARTZ_STRUCTURES_BST_H

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

#define QZ_BST_GREATER (1)
#define QZ_BST_LESS (-1)
#define QZ_BST_EQUAL (0)

typedef struct qz_bst_node qz_Bst_Node;
typedef struct qz_bst qz_Bst;

typedef int qz_Bst_Compare_Result;
typedef qz_Bst_Compare_Result (*qz_Bst_Compare_Func)(void*, void*);

struct qz_bst_node {
    struct qz_bst_node *left;
    struct qz_bst_node *right;

    void *info;
};

struct qz_bst {
    struct qz_bst_node *head;

    size_t info_size;
    qz_Bst_Compare_Func compare_func;
};

qz_Bst* qz_bst_create(size_t info_size, qz_Bst_Compare_Func compare_func);
void qz_bst_insert(qz_Bst *qzbst, void *info);
void qz_bst_destroy(qz_Bst *qzbst);

void qz_bst_get_info(qz_Bst *qzBst, void *info, void **resp);
void qz_bst_get_info_max(qz_Bst *qzbst, void **resp);
void qz_bst_get_info_min(qz_Bst *qzbst, void **resp);

bool qz_bst_exits_info(qz_Bst *qzBst, void *info);

qz_Bst* qz_bst_get(qz_Bst *qzbst, void *info);
qz_Bst* qz_bst_copy(qz_Bst *qzbst);

#endif //QUARTZ_STRUCTURES_BST_H
